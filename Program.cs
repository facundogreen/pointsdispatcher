﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;


class PointsStore
{
    List<float[]> allPoints = new List<float[]>();
    List<List<int>> queuesPoints = new List<List<int>>();
    List<int> queuesDistances = new List<int>();

    /// <summary>
    /// Add a point to the store and to the subscribed queues
    /// </summary>
    public void put(string point)
    {
        string[] pointParts = point.Split(':');
        float lat = float.Parse(pointParts[1]);
        float lon = float.Parse(pointParts[2]);
        float[] point_coords = new float[2];
        point_coords[0] = lat;
        point_coords[1] = lon;

        Console.WriteLine("Adding point to store...");
        allPoints.Add(point_coords);

        Console.WriteLine("Updating queues...");

        double distance = Math.Sqrt((double)(lat * lat  + lon * lon));

        List<int> affectedQueues = new List<int>();
        for (int i = 0; i < queuesDistances.Count; i++)
        {
            int queueDistance = queuesDistances[i];

            if (queueDistance <= distance)
            {
                affectedQueues.Add(i);
            }
        }

        foreach (int queueId in affectedQueues)
        {
            int pointId = allPoints.Count - 1;
            queuesPoints[queueId].Add(pointId);
        }

    }

    /// <summary>
    /// Get the points from a queue and clear it
    /// </summary>
    public List<float[]> get(int queueId)
    {
        Console.WriteLine("Getting queue {0}...", queueId);

        List<int> queue = queuesPoints[queueId];

        List<float[]> queuePoints = new List<float[]>();

        foreach (int pointId in queue)
        {
            queuePoints.Add(allPoints[pointId]);
        }

        queue.Clear();
        
        return queuePoints;
    }

    /// <summary>
    /// Create a new queue
    /// </summary>
    public int sub(int distance)
    {
        Console.WriteLine("Subscribing to max distance {0}...", distance);

        queuesDistances.Add(distance);

        queuesPoints.Add(new List<int>());

        return queuesDistances.Count;
    }
}


class PointsDispatcher
{
    /// <summary>
    /// Return the points in the queue and clear the queue
    /// </summary>
    static void SendPoints(NetworkStream stream, List<float[]> points)
    {
        foreach (float[] point in points)
        {
            string pointString = point[0].ToString("0.0000") + ":" + point[1].ToString("0.0000") + "\n";
            byte[] pointBytes = System.Text.Encoding.UTF8.GetBytes(pointString);
            stream.Write(pointBytes, 0, pointBytes.Length);
            Console.WriteLine("{0}", point);
        }
    }

    /// <summary>
    /// Create a new queue and subscribe to it
    /// </summary>
    static int AddQueue(PointsStore pointsStore, int distance)
    {
        int queueId = pointsStore.sub(distance);
        return queueId;
    }

    /// <summary>
    /// Return an error response
    /// </summary>
    static void SendError(NetworkStream stream, string errorString)
    {
            byte[] errorMsg = System.Text.Encoding.UTF8.GetBytes(errorString);
            stream.Write(errorMsg, 0, errorMsg.Length);
    }

    public static void Main()
    { 
        char put_command = 'P';
        char get_command = 'G';
        char sub_command = 'S';

        TcpListener server=null;   
        PointsStore pointStore = new PointsStore();

        try
        {
            Int32 port = 42024;
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");

            server = new TcpListener(localAddr, port);

            server.Start();

            Byte[] bytes = new Byte[256];
            string data = null;

            while(true) 
            {
                Console.Write("Waiting for a connection... ");

                TcpClient client = server.AcceptTcpClient();            
                Console.WriteLine("Connected!");

                data = null;

                NetworkStream stream = client.GetStream();

                int i;

                while((i = stream.Read(bytes, 0, bytes.Length)) != 0) 
                {   
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                    Console.WriteLine("Received: {0}", data);

                    data = data.ToUpper();

                    if (data[0] == put_command)
                    {
                        string[] commandParts = data.Split(':');

                        if (commandParts.Length < 3) {
                            SendError(stream, "Bad request: must specify point values (P:lat:lon)\n");
                        } else {
                            pointStore.put(data);
                        }
                    }
                    else if (data[0] == get_command)
                    {
                        Console.WriteLine("Getting points... ");

                        string[] commandParts = data.Split(':');

                        if (commandParts.Length < 2) {
                            SendError(stream, "Bad request: must specify queueId (G:queueId)\n");
                        } else {
                            int queueId = int.Parse(commandParts[1]);
                        
                            try
                            {
                                List<float[]> points = pointStore.get(queueId);

                                SendPoints(stream, points);
                            }
                            catch (System.ArgumentOutOfRangeException)
                            {
                                SendError(stream, "Bad request: queue not found\n");
                            }

                        }
                    }
                    else if (data[0] == sub_command)
                    {
                        Console.WriteLine("Subscribing ... ");

                        string[] commandParts = data.Split(':');

                        if (commandParts.Length < 2) {
                            SendError(stream, "Bad request: must specify distance (S:distance)\n");
                        } else {
                            int distance = int.Parse(commandParts[1]);
                            AddQueue(pointStore, distance);
                        }
                    }
                    else
                    {
                        SendError(stream, "Bad request: Unknown message type\n");
                    }
                }

                client.Close();
            }
        }
        catch(SocketException e)
        {
            Console.WriteLine("SocketException: {0}", e);
        }
        finally
        {
            // Stop listening for new clients.
            server.Stop();
        }

        Console.WriteLine("\nHit enter to continue...");
        Console.Read();
    }   
}
