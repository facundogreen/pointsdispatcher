=================
Points Dispatcher
=================

El servicio recibe información de puntos y permite que quienes esten interesados se subscriban
para recibir los puntos nuevos.

El serivcio escucha en el puerto TCP 42024.

Recibir puntos
--------------

El formato de los mensajes para recibir los puntos es

"P:{latitud}:{longitud}"

Los valores estan separados por ":"

El primer caracter "P" indica que el mensaje es un punto.

El segundo valor, latitud, se expresa como un numero de punto flotante.

El tercer valor, longitud, se expresa como un numero de punto flotante.

Basado en ISO 6709


Subscribirse a puntos
---------------------

El mensaje para subscribirse a los puntos es

"S:{latitud origen}:{longitud origen}:{distancia}"

Esto crea una cola de con los puntos que esten a una distancia igual o menor de la especificada
desde el origen especificado.

Recibir puntos
--------------

Para recibir los puntos almacenados en la cola se debe enviar un mensaje con

"G:{latitud origen}:{longitud origen}:{distancia}"

La respuesta es una lista con los puntos, separada por ";".

Cada punto es una tupla "{longitud}:{latitud}"
